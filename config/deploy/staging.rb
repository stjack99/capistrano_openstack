require_relative '../../lib/openstack'

#server 'myapp-web-s1.somewhere.com', roles: [:web], :primary => true

# Setup Openstack module
openstack_config 'staging'
openstack = Openstack::Nova.new

# Get list of instances
web_ips = openstack.get_ips_by_name 'myapp-web-s'  # Web
app_ips = openstack.get_ips_by_name 'myapp-app-s'  # App

# Define Web servers
web_ips.each do |instance|
  server instance, user: 'user_name', roles: [:web]
end

# Define App servers
app_ips.each do |instance|
  server instance, user: 'user_name', roles: [:app]
end


set :rails_env, 'staging'
set :branch, proc { 'develop' }

set :bundle_env_variables, {'NOKOGIRI_USE_SYSTEM_LIBRARIES' => 1}

set :default_env, {
    'PATH' => "/usr/local/rbenv/shims:/usr/local/rbenv/bin:$PATH"
}
