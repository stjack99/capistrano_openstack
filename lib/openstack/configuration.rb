module Openstack

  class Configuration

    attr_accessor :identity_url,
                  :password,
                  :tenantName,
                  :username

  end

end
