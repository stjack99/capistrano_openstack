require 'json'
require 'rest-client'

module Openstack

  class Nova

    def get_ips_by_name (name_prefix)

      identity = Openstack::Identity.new
      token_id = identity.get_token_id
      @compute_url = identity.get_service_endpoint 'nova'

      @servers_ip = []

      # Get a list of instances running in compute
      server_list = JSON.parse(RestClient.get "#{@compute_url}/servers", {:'X-Auth-Token' => token_id, :accept => :json})

      # Target specific instances
      server_list['servers'].each do |server|

        # Only if the name starts with name_prefix
        if server['name'].to_s.start_with? name_prefix

          # Get details about the instance
          _server = JSON.parse(RestClient.get "#{@compute_url}/servers/#{server['id']}", {:'X-Auth-Token' => token_id, :accept => :json})

          # Only if the instance is in an active state
          if _server['server']['status'].to_s.downcase == 'active'

            # For each network
            _server['server']['addresses'].keys.each do |net|

              # For each adapter
              _server['server']['addresses'][net].each do |address|

                # Only if it has a floating ip
                if address['OS-EXT-IPS:type'].to_s.downcase == 'floating'
                  @servers_ip << address['addr']
                end

              end

            end

          end

        end

      end

      return @servers_ip

    end

  end

end
