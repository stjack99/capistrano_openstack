require_relative 'openstack/configuration'
require_relative 'openstack/identity'
require_relative 'openstack/nova'

module Openstack

  # configuration setings
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end

  def self.configure_from_environment
    Openstack.configure do |config|
      config.identity_url = ENV['OS_AUTH_URL']
      config.password     = ENV['OS_PASSWORD']
      config.tenantName   = ENV['OS_TENANT_NAME']
      config.username     = ENV['OS_USERNAME']
    end

    return true
  end

end


