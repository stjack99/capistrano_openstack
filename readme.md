# Openstack Integration With Capistrano

A module that will query openstack for instances beginning with a specified name.

IP address are returned, but only for valid instances. Instances must:
* Be active and running
* Have a floating IP assigned

## Config

Place a openstack_config.yml file in config/secrets. This file will provide this module with the credentials and info it needs to connect to your openstack environment.

```yaml
development:
  identity_url: http://openstack-controller:5000/v2.0
  password: password
  tenantName: myproject
  username: username

production:
  identity_url: http://openstack-controller:5000/v2.0
  password: password
  tenantName: myproject
  username: username

staging:
  identity_url: http://openstack-controller:5000/v2.0
  password: password
  tenantName: myproject
  username: username

test:
  identity_url: http://openstack-controller:5000/v2.0
  password: password
  tenantName: myproject
  username: username
```

## Capistrano Deployment

Call openstack.get_ips_by_name and pass in the prefix to the list of instances you want. It will return an array of IP address to valid instances. Then just define a role like normal for each one.

```ruby
require_relative '../../lib/openstack'

# Setup Openstack module
openstack_config 'production'
openstack = Openstack::Nova.new

# Get list of instances
web_ips = openstack.get_ips_by_name 'myapp-web-p'  # Web
app_ips = openstack.get_ips_by_name 'myapp-app-p'  # App

# Define Web servers
web_ips.each do |instance|
  server instance, user: 'user_name', roles: [:web]
end

# Define App servers
app_ips.each do |instance|
  server instance, user: 'user_name', roles: [:app]
end
```
