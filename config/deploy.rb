# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'myapp'
set :deploy_user, 'user_name'

set :keep_releases, 5

set :repo_url, "ssh://git@some-url.git"
set :deploy_via, :remote_cache

set :use_sudo, false
set :ssh_options, {
    user: 'user_name',
    forward_agent: true,
    keys: [File.join('/home/user_name/.ssh/id_rsa')]
}
set :deploy_to, '/path'                                                                       # Default deploy_to directory is /var/www/my_app
set :linked_files, %w{config/database.yml config/etc}                                         # Default value for :linked_files is []
set :log_level, :debug                                                                        # Default value for :log_level is :debug

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:web), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  desc "Combine finish work"
  task :finish_work => ["deploy:restart" ]

  after :finishing, "deploy:finish_work"

end

####################################
# Capistrano - freebsd hack; issue will eventually go away
# https://github.com/capistrano/capistrano/issues/677
####################################

module GitStrategy
  require 'capistrano/git'
  include Capistrano::Git::DefaultStrategy

  def release
    git :archive, fetch(:branch), '| tar -x -f - -C', release_path
  end
end

set :git_strategy, GitStrategy

# Setup openstack module config
def openstack_config (rails_env)

  openstack_config = YAML.load_file('config/secrets/openstack_config.yml')[rails_env]

  Openstack.configure do |config|
    config.identity_url = openstack_config['identity_url']
    config.password     = openstack_config['password']
    config.tenantName   = openstack_config['tenantName']
    config.username     = openstack_config['username']
  end

end
