require 'json'
require 'rest-client'

module Openstack

  class Identity
    attr_accessor :identity

    def get_token_id

      if @identity.nil?
        get_identity
      end
      # Set auth token

      return @identity['access']['token']['id']

    end


    def get_service_endpoint(service_name)

      service_endpoints = get_service_endpoints

      return service_endpoints[service_name]

    end


    def get_service_endpoints

      if @identity.nil?
        get_identity
      end

      service_endpoints = Hash.new

      # Get service API URLs
      @identity['access']['serviceCatalog'].each do |serviceCatalog|
        service_endpoints[serviceCatalog['name']] = serviceCatalog['endpoints'][0]['internalURL']
      end

      return service_endpoints

    end


    private

    def get_identity

      # Keystone identity API URL and credentials

      payload   = {
        :auth => {
          :tenantName          => Openstack.configuration.tenantName,
          :passwordCredentials => {
            :username => Openstack.configuration.username,
            :password => Openstack.configuration.password
          }
        }
      }

      # Get identity package, including token
      @identity = JSON.parse(RestClient.post "#{Openstack.configuration.identity_url}/tokens", payload.to_json, :content_type => :json, :accept => :json)

    end

  end

end
