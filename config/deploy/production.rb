require_relative '../../lib/openstack'

#server 'myapp-web-p1.somewhere.com', user: 'user_name', roles: [:web], :primary => true

# Setup Openstack module
openstack_config 'production'
openstack = Openstack::Nova.new

# Get list of instances
web_ips = openstack.get_ips_by_name 'myapp-web-p'  # Web
app_ips = openstack.get_ips_by_name 'myapp-app-p'  # App

# Define Web servers
web_ips.each do |instance|
  server instance, user: 'user_name', roles: [:web]
end

# Define App servers
app_ips.each do |instance|
  server instance, user: 'user_name', roles: [:app]
end

set :rails_env, 'production'
set :branch, 'master'

set :bundle_env_variables, {'NOKOGIRI_USE_SYSTEM_LIBRARIES' => 1}

set :default_env, {
    'PATH' => "/usr/local/rbenv/shims:/usr/local/rbenv/bin:$PATH"
}
